# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'childForm.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_childForm(object):
    def setupUi(self, childForm):
        childForm.setObjectName("childForm")
        childForm.resize(475, 329)
        self.centralwidget = QtWidgets.QWidget(childForm)
        self.centralwidget.setObjectName("centralwidget")
        self.textEdit_2 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_2.setGeometry(QtCore.QRect(0, 0, 471, 271))
        self.textEdit_2.setObjectName("textEdit_2")
        childForm.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(childForm)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 475, 25))
        self.menubar.setObjectName("menubar")
        childForm.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(childForm)
        self.statusbar.setObjectName("statusbar")
        childForm.setStatusBar(self.statusbar)

        self.retranslateUi(childForm)
        QtCore.QMetaObject.connectSlotsByName(childForm)

    def retranslateUi(self, childForm):
        _translate = QtCore.QCoreApplication.translate
        childForm.setWindowTitle(_translate("childForm", "directions"))
        self.textEdit_2.setHtml(_translate("childForm", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'PMingLiU\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">遊戲說明:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">每人開始皆有20000的籌碼，可以點選各綠色區塊來選擇要投入的籌碼數，各區塊讓滑鼠停留一段時間，皆有賠率提示。</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">按CTRL+R或按左上功能-重玩便能重新開一局。</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">按CTRL+V或按左上功能-說明便能重啟我喔。</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">大(11~17):1賠1</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">小(4~10):1賠1</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">一個骰子(1~6):1賠1</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">兩個骰子不重複組合:1賠5</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">三個數字和:請看提示</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">兩個骰子重複(1~6):1賠8</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">三個骰子重複(1~6):1賠150</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">三個骰子重複組合型態:1賠24</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600; text-decoration: underline;\">最後按下&quot;投注完畢&quot;或&quot;快捷鍵:空白鍵&quot;開始擲骰。</span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    childForm = QtWidgets.QMainWindow()
    ui = Ui_childForm()
    ui.setupUi(childForm)
    childForm.show()
    sys.exit(app.exec_())
