//0301_T12345678
#include <stdio.h>
#include <stdlib.h>
int main(void){
    
int a,b,c;  //三角形三邊長 
int flag=1;  //標記、flag=1
	
scanf("%d %d %d",&a,&b,&c);

//兩邊和小於或等於第三邊、則flag=0	
if(a+b<=c) flag=0;  
if(b+c<=a) flag=0;
if(a+c<=b) flag=0;
		
//兩邊平方和等於第三邊平方、則flag=2
if(a*a+b*b==c*c) flag=2;  
if(b*b+c*c==a*a) flag=2;
if(c*c+a*a==b*b) flag=2;
    
if(flag==0) printf("0\n");  //不是三角形 
if(flag==1) printf("1\n");  //是三角形,但不是直角三角形
if(flag==2) printf("2\n");  //是直角三角形

system("pause");return 0;}
//判斷三角形或直角三角形
