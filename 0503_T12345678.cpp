//0503_T12345678
#include<stdio.h>
#include<stdlib.h>
int main(void){
    
int x,n=1,flag=1,d=1;
    
scanf("%d",&x);
    
loop1:
 if( (x/n)/10 >0 )  //找出輸入為多少位數 n
  {d++; n=n*10;goto loop1;}

printf("%d\n",d); 

loop2:
 if( x/n != x%10)  //比較 最左邊一位數字和最右邊一位數字
  flag=0;          //有不相同則標記為0
			
				   //將比較過的數字去除
  x=x%n;           //x%n 去除最左邊一位數字
  x=x/10;          //x/n 去除最右邊一位數字
		
  n=n/100;         //每次減少2位數
		
if( x>0 && flag==1) goto loop2;  //若 x 已比較完畢 
                                 //或已經有不同位數，則結束
		
printf("%d\n",flag);  //印出結果
    
system("pause");return 0;}
//輸入為多少位數和是否為迴文數(goto)_正確版本
