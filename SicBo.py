import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from threading import Timer
import random
from mainMenu import Ui_mainWindow
from childForm import Ui_childForm

class MainForm(QtWidgets.QMainWindow,Ui_mainWindow):
    #各按鈕行為
        def __init__(self,parent=None):
                super().__init__()
                self.setupUi(self)
                self.pushButton.clicked.connect(self.startgameing)
                self.pushButton.setShortcut("Space")
                self.CLICK1=1
                self.p1txt.setReadOnly(True)
                self.p2txt.setReadOnly(True)
                self.p3txt.setReadOnly(True)
                self.p1txt_2.setReadOnly(True)
                self.p4txt.setReadOnly(True)
                self.a1.clicked.connect(self.getInta1)
                self.a2.clicked.connect(self.getInta2)
                self.a3.clicked.connect(self.getInta3)
                self.a4.clicked.connect(self.getInta4)
                self.a5.clicked.connect(self.getInta5)
                self.a6.clicked.connect(self.getInta6)
                self.c12.clicked.connect(self.getIntc12)
                self.c13.clicked.connect(self.getIntc13)
                self.c14.clicked.connect(self.getIntc14)
                self.c15.clicked.connect(self.getIntc15)
                self.c16.clicked.connect(self.getIntc16)
                self.c23.clicked.connect(self.getIntc23)
                self.c24.clicked.connect(self.getIntc24)
                self.c25.clicked.connect(self.getIntc25)
                self.c26.clicked.connect(self.getIntc26)
                self.c34.clicked.connect(self.getIntc34)
                self.c35.clicked.connect(self.getIntc35)
                self.c36.clicked.connect(self.getIntc36)
                self.c45.clicked.connect(self.getIntc45)
                self.c46.clicked.connect(self.getIntc46)
                self.c56.clicked.connect(self.getIntc56)
                self.b4.clicked.connect(self.getIntb4)
                self.b5.clicked.connect(self.getIntb5)
                self.b6.clicked.connect(self.getIntb6)
                self.b7.clicked.connect(self.getIntb7)
                self.b8.clicked.connect(self.getIntb8)
                self.b9.clicked.connect(self.getIntb9)
                self.b10.clicked.connect(self.getIntb10)
                self.b11.clicked.connect(self.getIntb11)
                self.b12.clicked.connect(self.getIntb12)
                self.b13.clicked.connect(self.getIntb13)
                self.b14.clicked.connect(self.getIntb14)
                self.b15.clicked.connect(self.getIntb15)
                self.b16.clicked.connect(self.getIntb16)
                self.b17.clicked.connect(self.getIntb17)
                self.a111.clicked.connect(self.getInta111)
                self.a222.clicked.connect(self.getInta222)
                self.a333.clicked.connect(self.getInta333)
                self.a444.clicked.connect(self.getInta444)
                self.a555.clicked.connect(self.getInta555)
                self.a666.clicked.connect(self.getInta666)
                self.a11.clicked.connect(self.getInta11)
                self.a22.clicked.connect(self.getInta22)
                self.a33.clicked.connect(self.getInta33)
                self.a44.clicked.connect(self.getInta44)
                self.a55.clicked.connect(self.getInta55)
                self.a66.clicked.connect(self.getInta66)
                self.A1.clicked.connect(self.getIntA1)
                self.A2.clicked.connect(self.getIntA2)
                self.A1_2.clicked.connect(self.getIntA1_2)
                self.A1_3.clicked.connect(self.getIntA1_3)
                self.menubar.triggered[QtWidgets.QAction].connect(self.processtrigger)
        #重玩(變回預設值)及說明(呼叫child)函式
        def processtrigger(self,q):
            if self.CLICK1==1:
                if q.text()=='重玩':
                    self.ta1.setText("0");self.ta2.setText("0");self.ta3.setText("0");self.ta4.setText("0");
                    self.ta5.setText("0");self.ta6.setText("0");self.tc12.setText("0");self.tc24.setText("0");
                    self.tc14.setText("0");self.tc23.setText("0");self.tc34.setText("0");self.tc25.setText("0");
                    self.tc35.setText("0");self.tc26.setText("0");self.tc45.setText("0");self.tc36.setText("0");
                    self.tc15.setText("0");self.tc16.setText("0");self.tc46.setText("0");self.tc56.setText("0");
                    self.tc13.setText("0");self.tb17.setText("0");self.tb16.setText("0");self.tb15.setText("0");
                    self.tb14.setText("0");self.tb13.setText("0");self.tb12.setText("0");self.tb11.setText("0");
                    self.tb10.setText("0");self.tb9.setText("0");self.tb8.setText("0");self.tb7.setText("0");
                    self.tb6.setText("0");self.tb5.setText("0");self.tb4.setText("0");self.ta111.setText("0");
                    self.ta222.setText("0");self.ta333.setText("0");self.ta444.setText("0");self.ta555.setText("0");
                    self.ta666.setText("0");self.ta11.setText("0");self.ta22.setText("0");self.ta33.setText("0");
                    self.ta44.setText("0");self.ta55.setText("0");self.ta66.setText("0");
                    self.TA1.setText("0");self.TA2.setText("0");self.TA1_2.setText("0");self.TA1_3.setText("0");
                    self.p1txt.setText('20000');self.p2txt.setText('20000');self.p3txt.setText('20000');self.p4txt.setText('20000');
                    self.p1txt_2.setText('20000');self.lineEdit.setText('0')
                if q.text()=='說明':
                    child.show()
        #輸入各值後，自己扣錢，桌上指定位置加錢，若要更改，則先退錢
        def getInta1(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:1","請輸入要投入的籌碼:",)
                if ok:
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta1.text())))
                        self.ta1.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')
        def getInta2(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:1","請輸入要投入的籌碼:",)
                if ok:
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta2.text())))
                        self.ta2.setText(str(num))                 
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')
        def getInta3(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:1","請輸入要投入的籌碼:",)
                if ok:
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta3.text())))
                        self.ta3.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')                       
        def getInta4(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:1","請輸入要投入的籌碼:",)
                if ok:                   
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta4.text())))
                        self.ta4.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')      
        def getInta5(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:1","請輸入要投入的籌碼:",)
                if ok:                  
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta5.text())))
                        self.ta5.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')  
        def getInta6(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:1","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta6.text())))
                        self.ta6.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')        
        def getIntc12(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc12.text())))
                        self.tc12.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')               
        def getIntc13(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc13.text())))
                        self.tc13.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')           
        def getIntc14(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc14.text())))
                        self.tc14.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')         
        def getIntc15(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc15.text())))
                        self.tc15.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getIntc16(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc16.text())))
                        self.tc16.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')        
        def getIntc23(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc23.text())))
                        self.tc23.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getIntc24(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc24.text())))
                        self.tc24.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getIntc25(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc25.text())))
                        self.tc25.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')                 
        def getIntc26(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                   
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc26.text())))
                        self.tc26.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getIntc34(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc34.text())))
                        self.tc34.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getIntc35(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc35.text())))
                        self.tc35.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getIntc36(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc36.text())))
                        self.tc36.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')                 
        def getIntc45(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc45.text())))
                        self.tc45.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')           
        def getIntc46(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc46.text())))
                        self.tc46.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getIntc56(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:5","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tc56.text())))
                        self.tc56.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')               
        def getIntb4(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb4.text())))
                        self.tb4.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getIntb5(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb5.text())))
                        self.tb5.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getIntb6(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb6.text())))
                        self.tb6.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getIntb7(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb7.text())))
                        self.tb7.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')         
        def getIntb8(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb8.text())))
                        self.tb8.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')              
        def getIntb9(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb9.text())))
                        self.tb9.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getIntb10(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb10.text())))
                        self.tb10.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getIntb11(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb11.text())))
                        self.tb11.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')               
        def getIntb12(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb12.text())))
                        self.tb12.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getIntb13(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb13.text())))
                        self.tb13.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getIntb14(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb14.text())))
                        self.tb14.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')          
        def getIntb15(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb15.text())))
                        self.tb15.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')      
        def getIntb16(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb16.text())))
                        self.tb16.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')          
        def getIntb17(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:50","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.tb17.text())))
                        self.tb17.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')        
        def getInta111(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:8","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta111.text())))
                        self.ta111.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')          
        def getInta222(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:8","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta222.text())))
                        self.ta222.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')             
        def getInta333(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:8","請輸入要投入的籌碼:",)
                if ok:                    
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta333.text())))
                        self.ta333.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')           
        def getInta444(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:8","請輸入要投入的籌碼:",)
                if ok:                   
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta444.text())))
                        self.ta444.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')          
        def getInta555(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:8","請輸入要投入的籌碼:",)
                if ok:                   
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta555.text())))
                        self.ta555.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')         
        def getInta666(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:8","請輸入要投入的籌碼:",)
                if ok:                  
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta666.text())))
                        self.ta666.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')          
        def getInta11(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                  
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta11.text())))
                        self.ta11.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')          
        def getInta22(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                 
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta22.text())))
                        self.ta22.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')        
        def getInta33(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta33.text())))
                        self.ta33.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')            
        def getInta44(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                  
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta44.text())))
                        self.ta44.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')       
        def getInta55(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                  
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta55.text())))
                        self.ta55.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')       
        def getInta66(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                 
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.ta66.text())))
                        self.ta66.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')        
        def getIntA1(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                 
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.TA1.text())))
                        self.TA1.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')         
        def getIntA2(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                 
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.TA2.text())))
                        self.TA2.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')          
        def getIntA1_2(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                 
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.TA1_2.text())))
                        self.TA1_2.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了')       
        def getIntA1_3(self):
            if self.CLICK1==1:
                num, ok =QtWidgets.QInputDialog.getInt(self, "1:150","請輸入要投入的籌碼:",)
                if ok:                 
                    if int(self.p1txt_2.text())>=num:
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())+int(self.TA1_3.text())))
                        self.TA1_3.setText(str(num))
                        self.p1txt_2.setText(str(int(self.p1txt_2.text())-num))
                    else:
                        QtWidgets.QMessageBox.warning(self,'Warning','您的籌碼不夠了') 
        #結束投注，動畫設定，儲存桌面上各位置的錢至money串列，及設定各電腦的投注值
        def startgameing(self):
                if self.CLICK1==1:
                    self.CLICK1=0
                    self.movie=QtGui.QMovie('./Chen/turn.gif')
                    self.movie.setSpeed(100)
                    self.Dice1.setMovie(self.movie)
                    self.Dice2.setMovie(self.movie)
                    self.Dice3.setMovie(self.movie)
                    self.movie.start()
                    self.anim = QtCore.QPropertyAnimation(self.DiceCup,b'geometry')
                    self.anim.setDuration(400)
                    self.anim.setLoopCount(5)
                    self.anim.setStartValue(QtCore.QRect(500,150,180,180))
                    self.anim.setEndValue(QtCore.QRect(460,200,180,180))
                    self.anim.start()
                    self.money=[int(self.ta1.text()),int(self.ta2.text()),int(self.ta3.text()),
                                int(self.ta4.text()),int(self.ta5.text()),int(self.ta6.text()),
                                int(self.tc12.text()),int(self.tc24.text()),int(self.tc14.text()),
                                int(self.tc23.text()),int(self.tc34.text()),int(self.tc25.text()),
                                int(self.tc35.text()),int(self.tc26.text()),int(self.tc45.text()),
                                int(self.tc36.text()),int(self.tc15.text()),int(self.tc16.text()),
                                int(self.tc46.text()),int(self.tc56.text()),int(self.tc13.text()),
                                int(self.tb17.text()),int(self.tb16.text()),int(self.tb15.text()),
                                int(self.tb14.text()),int(self.tb13.text()),int(self.tb12.text()),
                                int(self.tb11.text()),int(self.tb10.text()),int(self.tb9.text()),
                                int(self.tb8.text()),int(self.tb7.text()),int(self.tb6.text()),
                                int(self.tb5.text()),int(self.tb4.text()),int(self.ta111.text()),
                                int(self.ta222.text()),int(self.ta333.text()),int(self.ta444.text()),
                                int(self.ta555.text()),int(self.ta666.text()),int(self.ta11.text()),
                                int(self.ta22.text()),int(self.ta33.text()),int(self.ta44.text()),
                                int(self.ta55.text()),int(self.ta66.text()),int(self.TA1.text()),
                                int(self.TA2.text()),int(self.TA1_2.text()),int(self.TA1_3.text())]
                    self.p1ch=[0,0]#[位置,錢]
                    self.p2ch=[0,0]
                    self.p3ch=[0,0]
                    self.p4ch=[0,0]
                    self.p1ch[0]=random.randint(1,51)
                    self.p1ch[1]=random.randint(0,200000)
                    self.p2ch[0]=random.randint(1,51)
                    self.p2ch[1]=random.randint(0,200000)
                    self.p3ch[0]=random.randint(1,51)
                    self.p3ch[1]=random.randint(0,200000)
                    self.p4ch[0]=random.randint(1,51)
                    self.p4ch[1]=random.randint(0,200000)
                    if self.p1ch[1]>20000:
                        self.p1ch[1]=100
                    if self.p2ch[1]>20000:
                        self.p2ch[1]=100
                    if self.p3ch[1]>20000:
                        self.p3ch[1]=100
                    if self.p4ch[1]>20000:
                        self.p4ch[1]=100
                    if int(self.p1txt.text())<=self.p1ch[1]:
                        self.p1ch[1]=int(self.p1txt.text())
                    if int(self.p2txt.text())<=self.p2ch[1]:
                        self.p2ch[1]=int(self.p2txt.text())
                    if int(self.p3txt.text())<=self.p3ch[1]:
                        self.p3ch[1]=int(self.p3txt.text())
                    if int(self.p4txt.text())<=self.p4ch[1]:
                        self.p4ch[1]=int(self.p4txt.text())
                    #從1~51對應money串列的順序，比較好編寫
                    if self.p1ch[0]==1:
                        self.ta1.setText(str(int(self.ta1.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==2:
                        self.ta2.setText(str(int(self.ta2.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==3:
                        self.ta3.setText(str(int(self.ta3.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==4:
                        self.ta4.setText(str(int(self.ta4.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==5:
                        self.ta5.setText(str(int(self.ta5.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==6:
                        self.ta6.setText(str(int(self.ta6.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==7:
                        self.tc12.setText(str(int(self.tc12.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==8:
                        self.tc24.setText(str(int(self.tc24.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==9:
                        self.tc14.setText(str(int(self.tc14.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==10:
                        self.tc23.setText(str(int(self.tc23.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==11:
                        self.tc34.setText(str(int(self.tc34.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==12:
                        self.tc25.setText(str(int(self.tc25.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==13:
                        self.tc35.setText(str(int(self.tc35.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==14:
                        self.tc26.setText(str(int(self.tc26.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==15:
                        self.tc45.setText(str(int(self.tc45.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==16:
                        self.tc36.setText(str(int(self.tc36.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==17:
                        self.tc15.setText(str(int(self.tc15.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==18:
                        self.tc16.setText(str(int(self.tc16.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==19:
                        self.tc46.setText(str(int(self.tc46.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==20:
                        self.tc56.setText(str(int(self.tc56.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==21:
                        self.tc13.setText(str(int(self.tc13.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==22:
                        self.tb17.setText(str(int(self.tb17.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==23:
                        self.tb16.setText(str(int(self.tb16.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==24:
                        self.tb15.setText(str(int(self.tb15.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==25:
                        self.tb14.setText(str(int(self.tb14.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==26:
                        self.tb13.setText(str(int(self.tb13.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==27:
                        self.tb12.setText(str(int(self.tb12.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==28:
                        self.tb11.setText(str(int(self.tb11.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==29:
                        self.tb10.setText(str(int(self.tb10.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==30:
                        self.tb9.setText(str(int(self.tb9.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==31:
                        self.tb8.setText(str(int(self.tb8.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==32:
                        self.tb7.setText(str(int(self.tb7.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==33:
                        self.tb6.setText(str(int(self.tb6.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==34:
                        self.tb5.setText(str(int(self.tb5.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==35:
                        self.tb4.setText(str(int(self.tb4.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==36:
                        self.ta111.setText(str(int(self.ta111.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==37:
                        self.ta222.setText(str(int(self.ta222.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==38:
                        self.ta333.setText(str(int(self.ta333.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==39:
                        self.ta444.setText(str(int(self.ta444.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==40:
                        self.ta555.setText(str(int(self.ta555.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==41:
                        self.ta666.setText(str(int(self.ta666.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==42:
                        self.ta11.setText(str(int(self.ta11.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==43:
                        self.ta22.setText(str(int(self.ta22.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==44:
                        self.ta33.setText(str(int(self.ta33.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==45:
                        self.ta44.setText(str(int(self.ta44.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==46:
                        self.ta55.setText(str(int(self.ta55.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==47:
                        self.ta66.setText(str(int(self.ta66.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==48:
                        self.TA1.setText(str(int(self.TA1.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==49:
                        self.TA2.setText(str(int(self.TA2.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==50:
                        self.TA1_2.setText(str(int(self.TA1_2.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    elif self.p1ch[0]==51:
                        self.TA1_3.setText(str(int(self.TA1_3.text())+self.p1ch[1]))
                        self.p1txt.setText(str(int(self.p1txt.text())-self.p1ch[1]))
                    if self.p2ch[0]==1:
                        self.ta1.setText(str(int(self.ta1.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==2:
                        self.ta2.setText(str(int(self.ta2.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==3:
                        self.ta3.setText(str(int(self.ta3.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==4:
                        self.ta4.setText(str(int(self.ta4.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==5:
                        self.ta5.setText(str(int(self.ta5.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==6:
                        self.ta6.setText(str(int(self.ta6.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==7:
                        self.tc12.setText(str(int(self.tc12.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==8:
                        self.tc24.setText(str(int(self.tc24.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==9:
                        self.tc14.setText(str(int(self.tc14.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==10:
                        self.tc23.setText(str(int(self.tc23.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==11:
                        self.tc34.setText(str(int(self.tc34.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==12:
                        self.tc25.setText(str(int(self.tc25.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==13:
                        self.tc35.setText(str(int(self.tc35.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==14:
                        self.tc26.setText(str(int(self.tc26.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==15:
                        self.tc45.setText(str(int(self.tc45.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==16:
                        self.tc36.setText(str(int(self.tc36.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==17:
                        self.tc15.setText(str(int(self.tc15.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==18:
                        self.tc16.setText(str(int(self.tc16.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==19:
                        self.tc46.setText(str(int(self.tc46.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==20:
                        self.tc56.setText(str(int(self.tc56.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==21:
                        self.tc13.setText(str(int(self.tc13.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==22:
                        self.tb17.setText(str(int(self.tb17.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==23:
                        self.tb16.setText(str(int(self.tb16.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==24:
                        self.tb15.setText(str(int(self.tb15.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==25:
                        self.tb14.setText(str(int(self.tb14.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==26:
                        self.tb13.setText(str(int(self.tb13.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==27:
                        self.tb12.setText(str(int(self.tb12.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==28:
                        self.tb11.setText(str(int(self.tb11.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==29:
                        self.tb10.setText(str(int(self.tb10.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==30:
                        self.tb9.setText(str(int(self.tb9.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==31:
                        self.tb8.setText(str(int(self.tb8.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==32:
                        self.tb7.setText(str(int(self.tb7.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==33:
                        self.tb6.setText(str(int(self.tb6.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==34:
                        self.tb5.setText(str(int(self.tb5.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==35:
                        self.tb4.setText(str(int(self.tb4.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==36:
                        self.ta111.setText(str(int(self.ta111.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==37:
                        self.ta222.setText(str(int(self.ta222.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==38:
                        self.ta333.setText(str(int(self.ta333.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==39:
                        self.ta444.setText(str(int(self.ta444.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==40:
                        self.ta555.setText(str(int(self.ta555.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==41:
                        self.ta666.setText(str(int(self.ta666.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==42:
                        self.ta11.setText(str(int(self.ta11.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==43:
                        self.ta22.setText(str(int(self.ta22.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==44:
                        self.ta33.setText(str(int(self.ta33.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==45:
                        self.ta44.setText(str(int(self.ta44.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==46:
                        self.ta55.setText(str(int(self.ta55.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==47:
                        self.ta66.setText(str(int(self.ta66.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==48:
                        self.TA1.setText(str(int(self.TA1.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==49:
                        self.TA2.setText(str(int(self.TA2.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==50:
                        self.TA1_2.setText(str(int(self.TA1_2.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    elif self.p2ch[0]==51:
                        self.TA1_3.setText(str(int(self.TA1_3.text())+self.p2ch[1]))
                        self.p2txt.setText(str(int(self.p2txt.text())-self.p2ch[1]))
                    if self.p3ch[0]==1:
                        self.ta1.setText(str(int(self.ta1.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==2:
                        self.ta2.setText(str(int(self.ta2.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==3:
                        self.ta3.setText(str(int(self.ta3.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==4:
                        self.ta4.setText(str(int(self.ta4.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==5:
                        self.ta5.setText(str(int(self.ta5.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==6:
                        self.ta6.setText(str(int(self.ta6.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==7:
                        self.tc12.setText(str(int(self.tc12.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==8:
                        self.tc24.setText(str(int(self.tc24.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==9:
                        self.tc14.setText(str(int(self.tc14.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==10:
                        self.tc23.setText(str(int(self.tc23.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==11:
                        self.tc34.setText(str(int(self.tc34.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==12:
                        self.tc25.setText(str(int(self.tc25.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==13:
                        self.tc35.setText(str(int(self.tc35.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==14:
                        self.tc26.setText(str(int(self.tc26.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==15:
                        self.tc45.setText(str(int(self.tc45.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==16:
                        self.tc36.setText(str(int(self.tc36.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==17:
                        self.tc15.setText(str(int(self.tc15.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==18:
                        self.tc16.setText(str(int(self.tc16.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==19:
                        self.tc46.setText(str(int(self.tc46.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==20:
                        self.tc56.setText(str(int(self.tc56.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==21:
                        self.tc13.setText(str(int(self.tc13.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==22:
                        self.tb17.setText(str(int(self.tb17.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==23:
                        self.tb16.setText(str(int(self.tb16.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==24:
                        self.tb15.setText(str(int(self.tb15.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==25:
                        self.tb14.setText(str(int(self.tb14.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==26:
                        self.tb13.setText(str(int(self.tb13.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==27:
                        self.tb12.setText(str(int(self.tb12.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==28:
                        self.tb11.setText(str(int(self.tb11.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==29:
                        self.tb10.setText(str(int(self.tb10.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==30:
                        self.tb9.setText(str(int(self.tb9.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==31:
                        self.tb8.setText(str(int(self.tb8.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==32:
                        self.tb7.setText(str(int(self.tb7.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==33:
                        self.tb6.setText(str(int(self.tb6.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==34:
                        self.tb5.setText(str(int(self.tb5.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==35:
                        self.tb4.setText(str(int(self.tb4.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==36:
                        self.ta111.setText(str(int(self.ta111.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==37:
                        self.ta222.setText(str(int(self.ta222.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==38:
                        self.ta333.setText(str(int(self.ta333.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==39:
                        self.ta444.setText(str(int(self.ta444.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==40:
                        self.ta555.setText(str(int(self.ta555.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==41:
                        self.ta666.setText(str(int(self.ta666.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==42:
                        self.ta11.setText(str(int(self.ta11.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==43:
                        self.ta22.setText(str(int(self.ta22.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==44:
                        self.ta33.setText(str(int(self.ta33.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==45:
                        self.ta44.setText(str(int(self.ta44.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==46:
                        self.ta55.setText(str(int(self.ta55.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==47:
                        self.ta66.setText(str(int(self.ta66.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==48:
                        self.TA1.setText(str(int(self.TA1.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==49:
                        self.TA2.setText(str(int(self.TA2.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==50:
                        self.TA1_2.setText(str(int(self.TA1_2.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    elif self.p3ch[0]==51:
                        self.TA1_3.setText(str(int(self.TA1_3.text())+self.p3ch[1]))
                        self.p3txt.setText(str(int(self.p3txt.text())-self.p3ch[1]))
                    if self.p4ch[0]==1:
                        self.ta1.setText(str(int(self.ta1.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==2:
                        self.ta2.setText(str(int(self.ta2.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==3:
                        self.ta3.setText(str(int(self.ta3.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==4:
                        self.ta4.setText(str(int(self.ta4.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==5:
                        self.ta5.setText(str(int(self.ta5.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==6:
                        self.ta6.setText(str(int(self.ta6.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==7:
                        self.tc12.setText(str(int(self.tc12.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==8:
                        self.tc24.setText(str(int(self.tc24.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==9:
                        self.tc14.setText(str(int(self.tc14.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==10:
                        self.tc23.setText(str(int(self.tc23.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==11:
                        self.tc34.setText(str(int(self.tc34.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==12:
                        self.tc25.setText(str(int(self.tc25.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==13:
                        self.tc35.setText(str(int(self.tc35.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==14:
                        self.tc26.setText(str(int(self.tc26.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==15:
                        self.tc45.setText(str(int(self.tc45.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==16:
                        self.tc36.setText(str(int(self.tc36.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==17:
                        self.tc15.setText(str(int(self.tc15.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==18:
                        self.tc16.setText(str(int(self.tc16.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==19:
                        self.tc46.setText(str(int(self.tc46.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==20:
                        self.tc56.setText(str(int(self.tc56.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==21:
                        self.tc13.setText(str(int(self.tc13.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==22:
                        self.tb17.setText(str(int(self.tb17.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==23:
                        self.tb16.setText(str(int(self.tb16.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==24:
                        self.tb15.setText(str(int(self.tb15.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==25:
                        self.tb14.setText(str(int(self.tb14.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==26:
                        self.tb13.setText(str(int(self.tb13.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==27:
                        self.tb12.setText(str(int(self.tb12.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==28:
                        self.tb11.setText(str(int(self.tb11.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==29:
                        self.tb10.setText(str(int(self.tb10.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==30:
                        self.tb9.setText(str(int(self.tb9.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==31:
                        self.tb8.setText(str(int(self.tb8.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==32:
                        self.tb7.setText(str(int(self.tb7.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==33:
                        self.tb6.setText(str(int(self.tb6.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==34:
                        self.tb5.setText(str(int(self.tb5.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==35:
                        self.tb4.setText(str(int(self.tb4.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==36:
                        self.ta111.setText(str(int(self.ta111.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==37:
                        self.ta222.setText(str(int(self.ta222.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==38:
                        self.ta333.setText(str(int(self.ta333.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==39:
                        self.ta444.setText(str(int(self.ta444.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==40:
                        self.ta555.setText(str(int(self.ta555.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==41:
                        self.ta666.setText(str(int(self.ta666.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==42:
                        self.ta11.setText(str(int(self.ta11.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==43:
                        self.ta22.setText(str(int(self.ta22.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==44:
                        self.ta33.setText(str(int(self.ta33.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==45:
                        self.ta44.setText(str(int(self.ta44.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==46:
                        self.ta55.setText(str(int(self.ta55.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==47:
                        self.ta66.setText(str(int(self.ta66.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==48:
                        self.TA1.setText(str(int(self.TA1.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==49:
                        self.TA2.setText(str(int(self.TA2.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==50:
                        self.TA1_2.setText(str(int(self.TA1_2.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))
                    elif self.p4ch[0]==51:
                        self.TA1_3.setText(str(int(self.TA1_3.text())+self.p4ch[1]))
                        self.p4txt.setText(str(int(self.p4txt.text())-self.p4ch[1]))


                    t=Timer(2,self.DiceGame)
                    t.start()
        #判斷骰子最終點數，及從對應的組合得出相應應該收回的或繳出的錢，最後把桌面清空
        def DiceGame(self):
                self.movie.stop()
                self.profit=0
                p1=random.randint(1,6)
                p2=random.randint(1,6)
                p3=random.randint(1,6)
                if p1==1:
                    self.Dice1.setPixmap(QtGui.QPixmap('./Chen/1'))
                elif p1==2:
                    self.Dice1.setPixmap(QtGui.QPixmap('./Chen/2'))
                elif p1==3:
                    self.Dice1.setPixmap(QtGui.QPixmap('./Chen/3'))
                elif p1==4:
                    self.Dice1.setPixmap(QtGui.QPixmap('./Chen/4'))
                elif p1==5:
                    self.Dice1.setPixmap(QtGui.QPixmap('./Chen/5'))
                elif p1==6:
                    self.Dice1.setPixmap(QtGui.QPixmap('./Chen/6'))
                if p2==1:
                    self.Dice2.setPixmap(QtGui.QPixmap('./Chen/1'))
                elif p2==2:
                    self.Dice2.setPixmap(QtGui.QPixmap('./Chen/2'))
                elif p2==3:
                    self.Dice2.setPixmap(QtGui.QPixmap('./Chen/3'))
                elif p2==4:
                    self.Dice2.setPixmap(QtGui.QPixmap('./Chen/4'))
                elif p2==5:
                    self.Dice2.setPixmap(QtGui.QPixmap('./Chen/5'))
                elif p2==6:
                    self.Dice2.setPixmap(QtGui.QPixmap('./Chen/6'))
                if p3==1:
                    self.Dice3.setPixmap(QtGui.QPixmap('./Chen/1'))
                elif p3==2:
                    self.Dice3.setPixmap(QtGui.QPixmap('./Chen/2'))
                elif p3==3:
                    self.Dice3.setPixmap(QtGui.QPixmap('./Chen/3'))
                elif p3==4:
                    self.Dice3.setPixmap(QtGui.QPixmap('./Chen/4'))
                elif p3==5:
                    self.Dice3.setPixmap(QtGui.QPixmap('./Chen/5'))
                elif p3==6:
                    self.Dice3.setPixmap(QtGui.QPixmap('./Chen/6'))
                if p1==1 or p2==1 or p3==1:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[0]*2))
                    self.profit+=int(self.money[0]*2)
                    if self.p1ch[0]==1:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==1:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==1:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==1:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if p1==2 or p2==2 or p3==2:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[1]*2))
                    self.profit+=int(self.money[1]*2)
                    if self.p1ch[0]==2:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==2:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==2:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==2:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if p1==3 or p2==3 or p3==3:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[2]*2))
                    self.profit+=int(self.money[2]*2)
                    if self.p1ch[0]==3:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==3:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==3:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==3:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if p1==4 or p2==4 or p3==4:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[3]*2))
                    self.profit+=int(self.money[3]*2)
                    if self.p1ch[0]==4:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==4:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==4:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==4:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if p1==5 or p2==5 or p3==5:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[4]*2))
                    self.profit+=int(self.money[4]*2)
                    if self.p1ch[0]==5:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==5:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==5:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==5:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if p1==6 or p2==6 or p3==6:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[5]*2))
                    self.profit+=int(self.money[5]*2)
                    if self.p1ch[0]==6:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==6:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==6:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==6:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if (p1==1 and p2==2) or (p1==1 and p3==2) or (p2==1 and p3==2) \
                or (p2==1 and p1==2) or (p3==1 and p1==2) or (p3==1 and p2==2):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[6]*6))
                    self.profit+=int(self.money[6]*6)
                    if self.p1ch[0]==7:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==7:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==7:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==7:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==2 and p2==4) or (p1==2 and p3==4) or (p2==2 and p3==4) \
                or (p2==2 and p1==4) or (p3==2 and p1==4) or (p3==2 and p2==4):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[7]*6))
                    self.profit+=int(self.money[7]*6)
                    if self.p1ch[0]==8:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==8:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==8:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==8:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==1 and p2==4) or (p1==1 and p3==4) or (p2==1 and p3==4) \
                or (p2==1 and p1==4) or (p3==1 and p1==4) or (p3==1 and p2==4):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[8]*6))
                    self.profit+=int(self.money[8]*6)
                    if self.p1ch[0]==9:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==9:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==9:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==9:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==2 and p2==3) or (p1==2 and p3==3) or (p2==2 and p3==3) \
                or (p2==2 and p1==3) or (p3==2 and p1==3) or (p3==2 and p2==3):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[9]*6))
                    self.profit+=int(self.money[9]*6)
                    if self.p1ch[0]==10:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==10:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==10:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==10:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==3 and p2==4) or (p1==3 and p3==4) or (p2==3 and p3==4) \
                or (p2==3 and p1==4) or (p3==3 and p1==4) or (p3==3 and p2==4):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[10]*6))
                    self.profit+=int(self.money[10]*6)
                    if self.p1ch[0]==11:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==11:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==11:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==11:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==2 and p2==5) or (p1==2 and p3==5) or (p2==2 and p3==5) \
                or (p2==2 and p1==5) or (p3==2 and p1==5) or (p3==2 and p2==5):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[11]*6))
                    self.profit+=int(self.money[11]*6)
                    if self.p1ch[0]==12:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==12:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==12:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==12:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==3 and p2==5) or (p1==3 and p3==5) or (p2==3 and p3==5) \
                or (p2==3 and p1==5) or (p3==3 and p1==5) or (p3==3 and p2==5):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[12]*6))
                    self.profit+=int(self.money[12]*6)
                    if self.p1ch[0]==13:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==13:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==13:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==13:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==2 and p2==6) or (p1==2 and p3==6) or (p2==2 and p3==6) \
                or (p2==2 and p1==6) or (p3==2 and p1==6) or (p3==2 and p2==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[13]*6))
                    self.profit+=int(self.money[13]*6)
                    if self.p1ch[0]==14:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==14:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==14:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==14:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==4 and p2==5) or (p1==4 and p3==5) or (p2==4 and p3==5) \
                or (p2==4 and p1==5) or (p3==4 and p1==5) or (p3==4 and p2==5):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[14]*6))
                    self.profit+=int(self.money[14]*6)
                    if self.p1ch[0]==15:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==15:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==15:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==15:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==3 and p2==6) or (p1==3 and p3==6) or (p2==3 and p3==6) \
                or (p2==3 and p1==6) or (p3==3 and p1==6) or (p3==3 and p2==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[15]*6))
                    self.profit+=int(self.money[15]*6)
                    if self.p1ch[0]==16:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==16:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==16:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==16:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==1 and p2==5) or (p1==1 and p3==5) or (p2==1 and p3==5) \
                or (p2==1 and p1==5) or (p3==1 and p1==5) or (p3==1 and p2==5):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[16]*6))
                    self.profit+=int(self.money[16]*6)
                    if self.p1ch[0]==17:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==17:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==17:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==17:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==1 and p2==6) or (p1==1 and p3==6) or (p2==1 and p3==6) \
                or (p2==1 and p1==6) or (p3==1 and p1==6) or (p3==1 and p2==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[17]*6))
                    self.profit+=int(self.money[17]*6)
                    if self.p1ch[0]==18:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==18:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==18:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==18:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==4 and p2==6) or (p1==4 and p3==6) or (p2==4 and p3==6) \
                or (p2==4 and p1==6) or (p3==4 and p1==6) or (p3==4 and p2==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[18]*6))
                    self.profit+=int(self.money[18]*6)
                    if self.p1ch[0]==19:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==19:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==19:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==19:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==5 and p2==6) or (p1==5 and p3==6) or (p2==5 and p3==6) \
                or (p2==5 and p1==6) or (p3==5 and p1==6) or (p3==5 and p2==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[19]*6))
                    self.profit+=int(self.money[19]*6)
                    if self.p1ch[0]==20:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==20:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==20:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==20:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if (p1==1 and p2==3) or (p1==1 and p3==3) or (p2==1 and p3==3) \
                or (p2==1 and p1==3) or (p3==1 and p1==3) or (p3==1 and p2==3):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[20]*6))
                    self.profit+=int(self.money[20]*6)
                    if self.p1ch[0]==21:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*6))
                    if self.p2ch[0]==21:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*6))
                    if self.p3ch[0]==21:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*6))
                    if self.p4ch[0]==21:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*6))
                if p1+p2+p3==17:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[21]*61))
                    self.profit+=int(self.money[21]*61)
                    if self.p1ch[0]==22:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*61))
                    if self.p2ch[0]==22:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*61))
                    if self.p3ch[0]==22:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*61))
                    if self.p4ch[0]==22:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*61))
                if p1+p2+p3==16:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[22]*21))
                    self.profit+=int(self.money[22]*21)
                    if self.p1ch[0]==23:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*21))
                    if self.p2ch[0]==23:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*21))
                    if self.p3ch[0]==23:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*21))
                    if self.p4ch[0]==23:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*21))
                if p1+p2+p3==15:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[23]*19))
                    self.profit+=int(self.money[23]*19)
                    if self.p1ch[0]==24:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*19))
                    if self.p2ch[0]==24:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*19))
                    if self.p3ch[0]==24:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*19))
                    if self.p4ch[0]==24:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*19))
                if p1+p2+p3==14:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[24]*13))
                    self.profit+=int(self.money[24]*13)
                    if self.p1ch[0]==25:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*13))
                    if self.p2ch[0]==25:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*13))
                    if self.p3ch[0]==25:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*13))
                    if self.p4ch[0]==25:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*13))
                if p1+p2+p3==13:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[25]*9))
                    self.profit+=int(self.money[25]*9)
                    if self.p1ch[0]==26:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==26:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==26:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==26:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if p1+p2+p3==12:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[26]*7))
                    self.profit+=int(self.money[26]*7)
                    if self.p1ch[0]==27:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*7))
                    if self.p2ch[0]==27:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*7))
                    if self.p3ch[0]==27:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*7))
                    if self.p4ch[0]==27:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*7))
                if p1+p2+p3==11:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[27]*8))
                    self.profit+=int(self.money[27]*8)
                    if self.p1ch[0]==28:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*8))
                    if self.p2ch[0]==28:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*8))
                    if self.p3ch[0]==28:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*8))
                    if self.p4ch[0]==28:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*8))
                if p1+p2+p3==10:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[28]*7))
                    self.profit+=int(self.money[28]*7)
                    if self.p1ch[0]==29:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*7))
                    if self.p2ch[0]==29:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*7))
                    if self.p3ch[0]==29:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*7))
                    if self.p4ch[0]==29:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*7))
                if p1+p2+p3==9:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[29]*7))
                    self.profit+=int(self.money[29]*7)
                    if self.p1ch[0]==30:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*7))
                    if self.p2ch[0]==30:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*7))
                    if self.p3ch[0]==30:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*7))
                    if self.p4ch[0]==30:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*7))
                if p1+p2+p3==8:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[30]*9))
                    self.profit+=int(self.money[30]*9)
                    if self.p1ch[0]==31:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==31:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==31:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==31:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if p1+p2+p3==7:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[31]*13))
                    self.profit+=int(self.money[31]*13)
                    if self.p1ch[0]==32:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*13))
                    if self.p2ch[0]==32:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*13))
                    if self.p3ch[0]==32:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*13))
                    if self.p4ch[0]==32:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*13))
                if p1+p2+p3==6:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[32]*19))
                    self.profit+=int(self.money[32]*19)
                    if self.p1ch[0]==33:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*19))
                    if self.p2ch[0]==33:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*19))
                    if self.p3ch[0]==33:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*19))
                    if self.p4ch[0]==33:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*19))
                if p1+p2+p3==5:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[33]*21))
                    self.profit+=int(self.money[33]*21)
                    if self.p1ch[0]==34:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*21))
                    if self.p2ch[0]==34:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*21))
                    if self.p3ch[0]==34:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*21))
                    if self.p4ch[0]==34:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*21))
                if p1+p2+p3==4:
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[34]*61))
                    self.profit+=int(self.money[34]*61)
                    if self.p1ch[0]==35:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*61))
                    if self.p2ch[0]==35:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*61))
                    if self.p3ch[0]==35:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*61))
                    if self.p4ch[0]==35:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*61))
                if (p1==1 and p2==1) or (p1==1 and p3==1) or (p2==1 and p3==1):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[35]*9))
                    self.profit+=int(self.money[35]*9)
                    if self.p1ch[0]==36:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==36:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==36:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==36:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if (p1==2 and p2==2) or (p1==2 and p3==2) or (p2==2 and p3==2):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[36]*9))
                    self.profit+=int(self.money[36]*9)
                    if self.p1ch[0]==37:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==37:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==37:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==37:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if (p1==3 and p2==3) or (p1==3 and p3==3) or (p2==3 and p3==3):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[37]*9))
                    self.profit+=int(self.money[37]*9)
                    if self.p1ch[0]==38:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==38:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==38:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==38:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if (p1==4 and p2==4) or (p1==4 and p3==4) or (p2==4 and p3==4):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[38]*9))
                    self.profit+=int(self.money[38]*9)
                    if self.p1ch[0]==39:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==39:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==39:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==39:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if (p1==5 and p2==5) or (p1==5 and p3==5) or (p2==5 and p3==5):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[39]*9))
                    self.profit+=int(self.money[39]*9)
                    if self.p1ch[0]==40:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==40:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==40:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==40:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if (p1==6 and p2==6) or (p1==6 and p3==6) or (p2==6 and p3==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[40]*9))
                    self.profit+=int(self.money[40]*9)
                    if self.p1ch[0]==41:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*9))
                    if self.p2ch[0]==41:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*9))
                    if self.p3ch[0]==41:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*9))
                    if self.p4ch[0]==41:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*9))
                if (p1==1 and p2==1 and p3==1):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[41]*151))
                    self.profit+=int(self.money[41]*151)
                    if self.p1ch[0]==42:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*151))
                    if self.p2ch[0]==42:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*151))
                    if self.p3ch[0]==42:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*151))
                    if self.p4ch[0]==42:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*151))
                if (p1==2 and p2==2 and p3==2):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[42]*151))
                    self.profit+=int(self.money[42]*151)
                    if self.p1ch[0]==43:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*151))
                    if self.p2ch[0]==43:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*151))
                    if self.p3ch[0]==43:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*151))
                    if self.p4ch[0]==43:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*151))
                if (p1==3 and p2==3 and p3==3):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[43]*151))
                    self.profit+=int(self.money[43]*151)
                    if self.p1ch[0]==44:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*151))
                    if self.p2ch[0]==44:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*151))
                    if self.p3ch[0]==44:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*151))
                    if self.p4ch[0]==44:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*151))
                if (p1==4 and p2==4 and p3==4):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[44]*151))
                    self.profit+=int(self.money[44]*151)
                    if self.p1ch[0]==45:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*151))
                    if self.p2ch[0]==45:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*151))
                    if self.p3ch[0]==45:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*151))
                    if self.p4ch[0]==45:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*151))
                if (p1==5 and p2==5 and p3==5):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[45]*151))
                    self.profit+=int(self.money[45]*151)
                    if self.p1ch[0]==46:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*151))
                    if self.p2ch[0]==46:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*151))
                    if self.p3ch[0]==46:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*151))
                    if self.p4ch[0]==46:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*151))
                if (p1==6 and p2==6 and p3==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[46]*151))
                    self.profit+=int(self.money[46]*151)
                    if self.p1ch[0]==47:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*151))
                    if self.p2ch[0]==47:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*151))
                    if self.p3ch[0]==47:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*151))
                    if self.p4ch[0]==47:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*151))
                if (p1+p2+p3<=17 and p1+p2+p3>=11):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[47]*2))
                    self.profit+=int(self.money[47]*2)
                    if self.p1ch[0]==48:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==48:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==48:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==48:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if (p1+p2+p3<=10 and p1+p2+p3>=4):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[48]*2))
                    self.profit+=int(self.money[48]*2)
                    if self.p1ch[0]==49:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*2))
                    if self.p2ch[0]==49:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*2))
                    if self.p3ch[0]==49:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*2))
                    if self.p4ch[0]==49:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*2))
                if (p1==1 and p2==1 and p3==1) or (p1==2 and p2==2 and p3==2) or (p1==3 and p2==3 and p3==3):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[49]*25))
                    self.profit+=int(self.money[49]*25)
                    if self.p1ch[0]==50:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*25))
                    if self.p2ch[0]==50:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*25))
                    if self.p3ch[0]==50:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*25))
                    if self.p4ch[0]==50:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*25))
                if (p1==4 and p2==4 and p3==4) or (p1==5 and p2==5 and p3==5) or (p1==6 and p2==6 and p3==6):
                    self.p1txt_2.setText(str(int(self.p1txt_2.text())+self.money[50]*25))
                    self.profit+=int(self.money[50]*25)
                    if self.p1ch[0]==51:
                        self.p1txt.setText(str(int(self.p1txt.text())+self.p1ch[1]*25))
                    if self.p2ch[0]==51:
                        self.p2txt.setText(str(int(self.p2txt.text())+self.p2ch[1]*25))
                    if self.p3ch[0]==51:
                        self.p3txt.setText(str(int(self.p3txt.text())+self.p3ch[1]*25))
                    if self.p4ch[0]==51:
                        self.p4txt.setText(str(int(self.p4txt.text())+self.p4ch[1]*25))
                self.ta1.setText("0");self.ta2.setText("0");self.ta3.setText("0");self.ta4.setText("0");
                self.ta5.setText("0");self.ta6.setText("0");self.tc12.setText("0");self.tc24.setText("0");
                self.tc14.setText("0");self.tc23.setText("0");self.tc34.setText("0");self.tc25.setText("0");
                self.tc35.setText("0");self.tc26.setText("0");self.tc45.setText("0");self.tc36.setText("0");
                self.tc15.setText("0");self.tc16.setText("0");self.tc46.setText("0");self.tc56.setText("0");
                self.tc13.setText("0");self.tb17.setText("0");self.tb16.setText("0");self.tb15.setText("0");
                self.tb14.setText("0");self.tb13.setText("0");self.tb12.setText("0");self.tb11.setText("0");
                self.tb10.setText("0");self.tb9.setText("0");self.tb8.setText("0");self.tb7.setText("0");
                self.tb6.setText("0");self.tb5.setText("0");self.tb4.setText("0");self.ta111.setText("0");
                self.ta222.setText("0");self.ta333.setText("0");self.ta444.setText("0");self.ta555.setText("0");
                self.ta666.setText("0");self.ta11.setText("0");self.ta22.setText("0");self.ta33.setText("0");
                self.ta44.setText("0");self.ta55.setText("0");self.ta66.setText("0");
                self.TA1.setText("0");self.TA2.setText("0");self.TA1_2.setText("0");self.TA1_3.setText("0");
                self.lineEdit.setText(str(self.profit))
                self.CLICK1=1
class ChildrenForm(QtWidgets.QMainWindow,Ui_childForm):
        def __init__(self,parent=None):
            super().__init__()
            self.setupUi(self)
            self.textEdit_2.setReadOnly(True)



					
if __name__ == '__main__':
	app = QtWidgets.QApplication(sys.argv)
	demo = MainForm()
	demo.show()
	child = ChildrenForm() 
	child.show()
	sys.exit(app.exec_())
